import React, { Component } from 'react'

class Robot extends Component {
  constructor(props){
    super(props);
    this.state = {
      name : this.props.item.name,
      type : this.props.item.type,
      mass : this.props.item.mass, 
    }

  }
  render() {
  	let {item} = this.props
    return (
      <div>
  		Hello, my name is {item.name}. I am a {item.type} and weigh {item.mass}
      </div>
    )
  }
}

export default Robot
